let deduplicate = (rows) => {
    let cart = new Map();
    for (i in rows) {
        const row = rows[i]
        if (!(row.id in cart)) {
            cart[row.id] =  row
            row.quantity = 0
        }
        cart[row.id].quantity += 1
    }
    res = Object.keys(cart).map(key => (cart[key]));
    return Object.keys(cart).map(key => (cart[key]));
};


module.exports = deduplicate;